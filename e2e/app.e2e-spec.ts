import { BiomechanicToolsPage } from './app.po';

describe('biomechanic-tools App', function() {
  let page: BiomechanicToolsPage;

  beforeEach(() => {
    page = new BiomechanicToolsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
