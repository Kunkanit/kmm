import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { AboutComponent } from './about/about.component';
import { HomeComponent } from './home/home.component';
import { TutorialComponent } from './tutorial/tutorial.component';
import { SpreadSheetComponent } from './spread-sheet/spread-sheet.component';

export const router: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', component: HomeComponent },
    { path: 'about', component: AboutComponent },
    { path: 'spread-sheet', component: SpreadSheetComponent },
    { path: 'tutorial', component: TutorialComponent }
];

export const routes: ModuleWithProviders = RouterModule.forRoot(router);